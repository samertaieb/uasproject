package tn.uas.hello.springboot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Customer {
	private Long id ;
	private String fullName;
	private Integer age ;
	private Boolean idStudent ;
	public Customer(Long id, String fullName, Integer age, Boolean idStudent) {
		
		this.id = id;
		this.fullName = fullName;
		this.age = age;
		this.idStudent = idStudent;
	}
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Boolean getIdStudent() {
		return idStudent;
	}
	public void setIdStudent(Boolean idStudent) {
		this.idStudent = idStudent;
	}
}
