package tn.uas.hello.springboot;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

public class CustomerServiceImp implements customerService {
	@Autowired

	private CustomerRepository customerRepository;

	@Override
	public Customer create(Customer c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Customer update(Customer c) {
		 return customerRepository.save(c);
	}

	@Override
	public Customer readById(Long id) {
		  Optional<Customer> customerOptional = customerRepository.findById(id);
		return customerOptional.isPresent()? customerOptional.get() : null;
	}

	@Override
	public List<Customer> readAll() {
		 return  customerRepository.findAll();
	}

	@Override
	public void delete(Customer c) {
		customerRepository.delete(c);
		
	}

	@Override
	public void delete(Long id) {
		 customerRepository.deleteById(id);
		
	}

   

}