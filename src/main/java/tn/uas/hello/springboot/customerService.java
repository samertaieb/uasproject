package tn.uas.hello.springboot;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public interface customerService {
@Autowired
	Customer create (Customer c);
	Customer update(Customer c);
	Customer readById(Long id );
	List < Customer > readAll();
	 void delete (Customer c);
	 void delete (Long id);
	
}
